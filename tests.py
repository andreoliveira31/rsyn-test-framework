#!/usr/bin/python3
import sys
import subprocess
import os
import timeit
import argparse
import time
import csv
from config import *
from datetime import timedelta, datetime

# Instructions:
# Before running this script:
# > Create a logger in your program
# > > #include "private/ispd19/ufrgs/Test/Logger.h"
# > > In your initialization function request the logger using "logger = clsSession.getService("ufrgs.logger");"
# > > Create a wrapper function in your header, for example: "template<class T> inline void log(T obj) { if(this->logger) logger->w(obj); }"
# > > Add debug information to log by using, for example, 'log("[Error] Net has no pins.");'
# > Go to your script (NOT NECESSARY IF USING SCRIPT MAKER)
# > > Initialize the logger service using: start "ufrgs.logger"  { "debugVerbosity" : "1", "logFile" : "/home/asdeoliveira/Workspace/scripts/ispd/test/auto/logs/ispd19_test1.log" };
# > > Make sure to change the path of the log file to an existing path in your system.
# > Go to config.py
# > > Make sure that the rsyn path is correct (although this will be verified)
# > To run this script use: python3 ./tests.py [args]
# > If you need help or information about the arguments run: python3 ./tests.py -h
# > Example: python3 ./tests.py -mode fast -scripts-path ./auto -delay-start 60
# > Example: python3 ./tests.py -mode full -log-stdout true -number-of-prints 10 -delay-start 360 -memorize true


##### Argument parser ######
# Full documentation available at https://docs.python.org/2/library/argparse.html
parser = argparse.ArgumentParser(description='Automatic Rsyn Script tester.')
# The following lines define the 
# arguments the script may use.
parser.add_argument('-mode', help='Mode (fast, full, default)', default='default', choices=('default', 'fast', 'full'))
parser.add_argument('-number-of-prints', help='Number of warnings/errors printed per script', default=5, type=int)
parser.add_argument('-delay-start', help='Number of minutes to wait before starting the tests', default=0, type=int)
parser.add_argument('-scripts-path', help='Path to a directory containing the rsyn scripts', default='./auto')
parser.add_argument('-memorize', help='If true, the results will be appended to a new row in results.csv (file will be created if not exists).', default='false', choices=('true', 'false'))
parser.add_argument('-log-stdout', help='If true, the program output to stdout will be saved to a file.', default='false', choices=('true', 'false'))
# These lines read the arguments and
# initializes variables with their values
args = parser.parse_args()
mode = vars(args)['mode']
maximum_number_of_prints_per_script = vars(args)['number_of_prints']
delay_start = vars(args)['delay_start']
scripts_path = vars(args)['scripts_path']
memorize = vars(args)['memorize'] == 'true'
save_stdout = vars(args)['log_stdout'] == 'true'

def find_rsyn(path):
	try:
		exists = os.path.isfile(path)
		if exists:
			return True
		else:
			return False
	except Exception as e:
		print("Unable to find rsyn at " + path + ". Exception: " + e)
	

def run_test(script_filename):
	found_log = False
	for line in open(script_filename):
		if '"logFile"' in line:
			found_log = True
			break

	if not found_log:
		print(colors.WARNING + "Warning: " + script_filename + " contains no log options. For more information read the instructions in tests.py header." + colors.ENDC)
		
	command = rsyn_path + " " + args_draft % (script_filename) + " > "
	if save_stdout:
		command = command + script_filename + ".stdout.log"
	else:
		command = command + "/dev/null"
	try:
		start = timeit.default_timer()
		print("Running script " + script_filename + "." + colors.ENDC)
		process = subprocess.call(command, shell=True)
	except Exception as e:
		print(colors.FAIL + "Error running script " + script_filename + ": " + str(e.args) + colors.ENDC)
		return -1
	else:
		if process == 0:
			finish = timeit.default_timer()
			found_log = False
			log_filename = ""
			success = False
			error = False
			warning = False
			for line in open(script_filename):
				if '"logFile"' in line:
					found_log = True
					args = line.split('"')
					log_filename = args[len(args)-2]
					try:
						number_of_prints = 0
						for log_line in open(log_filename):
							for warning_string in strings_that_indicate_warning:
								if warning_string in log_line:
									number_of_prints += 1
									if number_of_prints == maximum_number_of_prints_per_script:
										print(colors.WARNING + "Omitting further warnings/errors because of limit" + colors.ENDC)
										continue
									elif number_of_prints > maximum_number_of_prints_per_script:
										continue
									print(colors.WARNING + "Log contains: [" + log_line + "]." + colors.ENDC)
									warning = True
							for error_string in strings_that_indicate_error:
								if error_string in log_line:
									number_of_prints += 1
									if number_of_prints == maximum_number_of_prints_per_script:
										print(colors.WARNING + "Omitting further warnings/errors because of limit" + colors.ENDC)
										continue
									elif number_of_prints > maximum_number_of_prints_per_script:
										continue
									print(colors.FAIL + "Log contains: [" + log_line + "]." + colors.ENDC)
									error = True
							for success_string in strings_that_indicate_success:
								if success_string in log_line:
									success = True
					except Exception as e:
						print(colors.FAIL + "Error opening log file " + log_filename + ": " + str(e.args) + colors.ENDC)
						return -1
			if error:
				return -1
			else:
				return 0
			if warning:
				print(colors.WARNING + "Warning running script " + script_filename + " Log file: file://" + log_filename + colors.ENDC)

		else:
			finish = timeit.default_timer()
			return -1


results = {}

if not find_rsyn(rsyn_path):
	print("Unable to find rsyn at " + rsyn_path + " - Exiting.")	
	exit(-1)
if os.path.isdir(scripts_path):
	if delay_start != 0:
		scheduled_time = datetime.now() + timedelta(minutes=delay_start)
		print(colors.HEADER + "Waiting for " + str(delay_start) + " minute(s) until " + scheduled_time.strftime('%Y-%m-%d %H:%M:%S') + " to start." + colors.ENDC)
		time.sleep(delay_start*60)
	print(colors.HEADER + "Running routing grid test script on " + mode + " mode." + colors.ENDC)
	test_start = timeit.default_timer()
	for filename in sorted(os.listdir(scripts_path)):
		if filename[len(filename)-1] == "~": continue
		if len(filename.split(".")) != 2: continue
		if filename.split(".")[1] != "rsyn": continue
		script_file = open(scripts_path + '/' + filename)
		script_speed = script_file.readline().strip()[1:]
		if script_speed == 'average' and mode == 'fast':
			print(colors.OKBLUE + "Skipping script " + filename + "." + colors.ENDC)
			continue
		if script_speed == 'slow' and mode == 'fast':
			print(colors.OKBLUE + "Skipping script " + filename + "." + colors.ENDC)
			continue
		if script_speed == 'slow' and mode == 'default':
			print(colors.OKBLUE + "Skipping script " + filename + "." + colors.ENDC)
			continue
		start = timeit.default_timer()
		result = run_test(scripts_path + '/' + filename)
		finish = timeit.default_timer()
		if result == 0:
			print(colors.OKGREEN + "Finished running script " + filename + ". Elapsed time: " + str(finish - start) + " seconds." + colors.ENDC)
			results[filename] = "Success"
		else:
			print(colors.FAIL + "Error running script " + filename + "." + colors.ENDC)
			results[filename] = "Fail"
			
	if memorize:
		print("Saving results to file.")
		results_data = []
		for run_result in results:					
			results_data.append((run_result, results[run_result]))
		with open('results.csv', 'a') as resultsdb:
			csv_writer = csv.writer(resultsdb)
			row_content = [datetime.now().strftime('%d:%m:%Y')] + sorted(results_data, key = lambda x: (x[0]))
			csv_writer.writerow(row_content)
			
			
else:
	print(colors.FAIL + "Could not find directory " + scripts_path + ". Exiting." + colors.ENDC)
	exit(-1)
test_finish = timeit.default_timer()
print(colors.HEADER + "Finished running all tests. Total elapsed time: " + str(test_finish - test_start) + " seconds." + colors.ENDC)
exit(0)



















