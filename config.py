# This file contains configuration variables
# used by rsyn_script_creator.py, script_maker.py
# and tests.py.
# Some configurations are optional and some are
# mandatory. They will be clearly labaled as
# such in the commentary describing the variable's use.

# Note: the arguments parser code is located
# in tests.py and not in this file



# Mandatory parameter that contains the 
# *absolute* path to rsyn binary
rsyn_path = '/home/asdeoliveira/Workspace/ispd19/build/release/bin/rsyn'

# Mandatory parameter that specifies arguments of the
# rsyn execution. 
# default: "-no-gui -script %s > /dev/null"
# '-no-gui' makes rsyn run in terminal only
# '-script %s' is necessary to specify the script
# '> /dev/null' hides the standard output of rsyn
args_draft = "-no-gui -script %s"

# Optional substrings that, when found in
# a log file, indicate success/errors/warnings
strings_that_indicate_warning = ['[Warning]']
strings_that_indicate_error = ['[Error]']
strings_that_indicate_success = []


# Mandatory list used by script_maker.py
# Write the name of the benchmarks in this list of tuples
# Each tuple contains:
# 0: Name of benchmark
# 1: Path to lef file
# 2: Path to def file
# 3: Path to guide file
# 4: Script speed (fast, average, slow)
# Note: existance of all files *will* be verified.
ispd19_benchmarks_path = '/home/asdeoliveira/Workspace/benchmarks/ispd19/'
path = ispd19_benchmarks_path
output_defs_path = '/home/asdeoliveira/ispd19_defs/'
benchmarks = [
	('ispd19_test1',
		path + 'ispd19_test1/ispd19_test1.input.lef', 
		path + 'ispd19_test1/ispd19_test1.input.def', 
		path + 'ispd19_test1/ispd19_test1.input.guide',
		'fast'),
	('ispd19_test2',
		path + 'ispd19_test2/ispd19_test2.input.lef', 
		path + 'ispd19_test2/ispd19_test2.input.def', 
		path + 'ispd19_test2/ispd19_test2.input.guide',
		'average'),
	('ispd19_test3',
		path + 'ispd19_test3/ispd19_test3.input.lef', 
		path + 'ispd19_test3/ispd19_test3.input.def', 
		path + 'ispd19_test3/ispd19_test3.input.guide',
		'fast'),
	('ispd19_test4',
		path + 'ispd19_test4/ispd19_test4.input.lef', 
		path + 'ispd19_test4/ispd19_test4.input.def', 
		path + 'ispd19_test4/ispd19_test4.input.guide',
		'average'),
	('ispd19_test5',
		path + 'ispd19_test5/ispd19_test5.input.lef', 
		path + 'ispd19_test5/ispd19_test5.input.def', 
		path + 'ispd19_test5/ispd19_test5.input.guide',
		'fast'),
	('ispd19_test6',
		path + 'ispd19_test6/ispd19_test6.input.lef', 
		path + 'ispd19_test6/ispd19_test6.input.def', 
		path + 'ispd19_test6/ispd19_test6.input.guide',
		'average'),
	('ispd19_test7',
		path + 'ispd19_test7/ispd19_test7.input.lef', 
		path + 'ispd19_test7/ispd19_test7.input.def', 
		path + 'ispd19_test7/ispd19_test7.input.guide',
		'slow'),
	('ispd19_test8',
		path + 'ispd19_test8/ispd19_test8.input.lef', 
		path + 'ispd19_test8/ispd19_test8.input.def', 
		path + 'ispd19_test8/ispd19_test8.input.guide',
		'slow'),
	('ispd19_test9',
		path + 'ispd19_test9/ispd19_test9.input.lef', 
		path + 'ispd19_test9/ispd19_test9.input.def', 
		path + 'ispd19_test9/ispd19_test9.input.guide',
		'slow'),
	('ispd19_test10',
		path + 'ispd19_test10/ispd19_test10.input.lef', 
		path + 'ispd19_test10/ispd19_test10.input.def', 
		path + 'ispd19_test10/ispd19_test10.input.guide',
		'slow')
	]
	
	
# Mandatory class with color codes for
# printing information with more readability.
# If possible, do not edit because other scripts
# depend on this class to print information.
class colors:
	HEADER = '\033[95m[INFO] '
	OKBLUE = '\033[94m[OK] '
	OKGREEN = '\033[92m[OK] '
	WARNING = '\033[93m[WARNING] '
	FAIL = '\033[91m[ERROR] '
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	
	
# Automatic evaluation data
# Mandatory: server address
server_address = 'ufrgs-server-10.inf.ufrgs.br'
server_username = 'andre.oliveira'
input_files_path = '~/ispd19/'

