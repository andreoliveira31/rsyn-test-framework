#!/bin/python3
# This script is used to evaluate the tool generated DEF file with Innovus using the UFRGS server.
# The script depends on information provided in config.py
# Overall flow:
# 1 - Send the tool generated DEF file over to the server.
# 2 - Run the contest evaluator script.
# 3 - Obtain the results of the evaluation and save them into a file.
# Note: This script requires that you can access the server remotely without a password.
# To achieve this, you need to create an ssh key and copy it over to the server.
# Instructions:
# Run: 
# $ ssh-keygen
# And follow the instructions to generate your key.
# Run:
# $ ssh-copy-id <username>@<server_address>
# Test to see if it works by using:
# $ ssh <username>@<server_address>
# If you can log in without being prompted the password it is properly configured.
#
# Running this script requires that the input files (i.e. *.input.guide, *.input.lef and *.input.def) are in 
# the given config.py path followed by the benchmark name.
# For example:
# ~/ispd19/ispd19_test*/*.input.*

import config
from config import *
import os
import subprocess

def send_file(file_path, file_name, server_address, username, remote_path):
	'''
	This function prepares a bash command to send over a file to a remote server location.
	Arguments: 
		file_path: The path to the directory where the file to be sent is located (ex. /home/user1/files/)
		file_name: The name of the file to be sent (ex. file1.txt)
		server_address: The address of the server where the file is being sent to (ex. ufrgs-server-10.inf.ufrgs.br)
		username: The username used to log into the remote server (ex. user1)
		remote_path: The path in the remote server to where the file is being sent (ex. /home/users/user1/)
	Returns:
		True if successfull, False if unsuccessful.		
	'''
	if not os.path.isdir(file_path):
		print(colors.FAIL + 'Path ' + file_path + ' could not be found.' + colors.ENDC)
		return False
	if not os.path.exists(file_path + '/' + file_name):
		print(colors.FAIL + 'File ' + file_name + ' could not be found.' + colors.ENDC)
		return False
	command = 'scp ' + file_path + '/' + file_name + ' ' + username + '@' + server_address + ':' + remote_path
	return_code = subprocess.call(command, shell=True)

	if return_code != 0:
		print(colors.WARNING + 'scp received return code ' + str(return_code) + colors.ENDC)	
		return False
	return True
	
def run_command(command, arguments, server_address, username):
	'''
	This function runs a bash command in a remote server.
	Arguments:
		command: The command that is going to be executed (ex. ls)
		arguments: The arguments for the command in a list of tuples (ex. [('-l', ''), ('>', 'log.txt')])
		server_address: The address of the server where the file is being sent to (ex. ufrgs-server-10.inf.ufrgs.br)
		username: The username used to log into the remote server (ex. user1)
	Returns:
		True if successfull, False if unsuccessful.
	'''
	command = command + ' '
	for argument, value in arguments:
		command = command + argument + ' ' + value + ' '
	command = 'ssh ' + username + '@' + server_address + ' "' + command + '"'
	print(command)
	return
	return_code = subprocess.call(command, shell=True)
	if return_code != 0:
		print(colors.WARNING + 'ssh received return code ' + str(return_code) + colors.ENDC)
		return False
	return True

def get_file(local_path, server_address, username, remote_path, file_name):
	'''
	This function prepares a bash command to get a file from a remote server location and move copy it to a directory in the local machine.
	Arguments:
		local_path: The path to the directory where the file is going to be saved in the local machine (ex. ~/saved_files/)
		server_address: The address of the server where the file is being obtained from (ex. ufrgs-server-10.inf.ufrgs.br)
		username: The username used to log into the remote server (ex. user1)
		remote_path: The path in the remote server to where the file is (ex. /home/users/user1/)
		file_name: The name of the file to be obtained (ex. file1.txt)
	Returns:
		True if successfull, False if unsuccessful.
	'''
	command = 'scp ' + username + '@' + server_address + ':' + remote_path + file_name + ' ' + local_path
	return_code = subprocess.call(command, shell=True)
	if return_code != 0:
		print(colors.WARNING + 'scp received return code ' + str(return_code) + colors.ENDC)
		return False
	return True
	
def main():
	for benchmark in config.benchmarks:
		output_def_path = config.output_defs_path
		output_def_filename = benchmark[0] + '.output.def'
		if not send_file(output_defs_path, output_def_filename, config.server_address, config.server_username, config.input_files_path + benchmark[0] + '/'):
			continue
		if not run_command('csh /scripts/set_cadence.csh; sh ~/ispd19eval/ispd19eval.sh -lef ' + config.input_files_path + benchmark[0] + '/' + benchmark[0] + '.input.lef -guide ' + config.input_files_path + benchmark[0] + '/' + benchmark[0] + '.input.guide -idef ' + config.input_files_path + benchmark[0] + '/' + benchmark[0] + '.input.def -odef ' + config.input_files_path + benchmark[0] + '/' + benchmark[0] + '.output.def', [], 'ufrgs-server-10.inf.ufrgs.br', 'andre.oliveira'):
			continue
		if not get_file('~/innovus_logs/', 'ufrgs-server-10.inf.ufrgs.br', 'andre.oliveira', '~/innovus_reports/', 'innovus_report_' + benchmark[0]):
			continue
		print(colors.OKGREEN + 'Completed evaluation of ' + benchmark[0] + '.' + colors.ENDC)
		
if __name__ == '__main__':
   main()
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
