import rsyn_script_creator
import os, errno
from config import *

# Instructions:
# > This script will create a rsyn script for each benchmark listed in config.py in the list called benchmarks
# > The existence of every file *is* verified.
# > If the script speed isn't defined in the last element of the tuple, it is marked as average by default.
# > To change the commands created in the rsyn script, use:
# > > creator.add_command(<command_name>, <command_target>, <list of argument tuples>)
# > For example:
# > > creator.add_command('start', 'ufrgs.logger', [  ('debugVerbosity', '1'),  ('logFile', '/home/asdeoliveira/logs/log.txt') ]  )
# > Some commands don't have a target or parameter list. In this case, use creator.add_simple_command(<command>)
# > For example:
# > creator.add_simple_command('writeDEFFile')


# Note:
# This script will check if there is a directory called ./auto
# If it finds the directory, it will continue using it.
# If it doesn't find it, it will try to create it.
# If it fails to create the directory, an exception will be raised
# This can be caused, for example, by not having the rights to create a directory in a specific path
# Currently, it is not possible to change the directory path to a custom one.

# Warning:
# This script *WILL OVERWRITE* the rsyn scripts in ./auto directory without asking for permission. Be careful.

try:
    os.makedirs('./auto')
except OSError as e:
    if e.errno != errno.EEXIST:
        raise
    
for benchmark in benchmarks:
	script_extension = '.rsyn'
	script_name = benchmark[0]
	script_full_name = script_name + script_extension
	if not os.path.isfile(benchmark[1]):
		print(colors.FAIL + 'File ' + benchmark[1] + ' could not be found. Skipping creation of script ' + script_name + colors.ENDC)
		continue
	if not os.path.isfile(benchmark[2]):
		print(colors.FAIL + 'File ' + benchmark[2] + ' could not be found. Skipping creation of script ' + script_name + colors.ENDC)
		continue
	if not os.path.isfile(benchmark[3]):
		print(colors.FAIL + 'File ' + benchmark[3] + ' could not be found. Skipping creation of script ' + script_name + colors.ENDC)
		continue
	creator = rsyn_script_creator.creator(path='./auto', filename = script_name)
	creator.add_comment(benchmark[4])
	creator.add_command('open', 'ispd18', [('lefFile', benchmark[1]), ('defFile', benchmark[2]), ('guideFile', benchmark[3])])
	creator.add_command('start', 'rsyn.writerDEF', [('path', output_defs_path), ('filename', benchmark[0] + '.output.def')])
	creator.add_command('start', 'ufrgs.logger', [('debugVerbosity', '1'), ('logFile', '/home/asdeoliveira/Workspace/scripts/ispd/test/auto/logs/' + benchmark[0] + '.log')])
	creator.add_command('start', 'rsyn.routingGrid', [])
	creator.add_command('start', 'ispd18.netlistManager', [])
#	creator.add_command('start', 'ufrgs.pinAccessGenerator', [])
#	creator.add_command('run', 'ufrgs.aStar', [])
	creator.add_command('run', 'ispd19.flow', [])
	creator.add_simple_command('writeDEFFile')
	if creator.generate_script():
		print(colors.OKBLUE + 'Successfully created rsyn script ' + script_name + '.' + colors.ENDC)
	else:
		print(colors.FAIL + 'Error creating rsyn script ' + script_name + '.' + colors.ENDC)
	
		


