import time
from datetime import datetime

class creator:
	def __init__(self, filename='a', extension='rsyn', path='./', comment_character='#', header = 'This script was generated automatically ' + datetime.now().strftime('at %H:%M:%S in %Y-%m-%d') + '.'):
		self.filename_ = filename
		self.extension_ = extension
		self.path_ = path
		self.comment_character_ = comment_character
		self.header_ = header
		self.commands_ = []
		self.comments_ = []
		
	def add_comment(self, text):
		self.comments_.append(self.comment_character_ + text + '\n')
	
	def add_command(self, command, target, arguments):
		command_string = command + ' "' + target + '" ' + ' { \n'
		for argument in arguments:
				command_string += '\t"' + argument[0] + '" : "' + argument[1] + '",\n'
		command_string = command_string[:-2]
		command_string += '\n};\n'
		self.commands_.append(command_string)
	
	def add_simple_command(self, command):
		self.commands_.append(command + ';')
		
	def generate_script(self):
		try:
			with open(self.path_ + '/' + self.filename_ + '.' + self.extension_, 'w+') as script_file:
				self.add_comment(self.header_)
				for comment in self.comments_:
					script_file.write(comment)
				for command in self.commands_:
					script_file.write(command)	
		except e as Exception:
			print(e)
			return False
		return True
